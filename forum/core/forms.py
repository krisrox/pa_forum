""" Forms """
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms
from django.forms import ModelForm
from .models import Category, Topic, Post


class CreateCategoryForm(ModelForm):
    """ Create category form """
    class Meta:
        """ Meta class """
        model = Category
        fields = ['name', 'order_number']


class TopicForm(ModelForm):
    """ Create topic form """
    class Meta:
        """ Meta class """
        model = Topic
        fields = ['name']


class PostForm(ModelForm):
    """ Create post form """
    class Meta:
        """ Meta class """
        model = Post
        fields = ['text']


class RegisterForm(UserCreationForm):
    """ User registration form """
    first_name = forms.CharField(max_length=30, required=False, help_text="Optional.")
    last_name = forms.CharField(max_length=30, required=False, help_text="Optional.")
    email = forms.EmailField(max_length=128, help_text="Required.")

    class Meta:
        """ Meta class """
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2')
