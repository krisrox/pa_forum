from django.test import TestCase, Client
from django.urls import reverse_lazy
from .test_mixins import TestMixin


class IndexViewTests(TestMixin, TestCase):
    def setUp(self) -> None:
        super(IndexViewTests, self).setUp()
        self.client = Client()

    def test_index_view_is_loaded(self):
        response = self.client.get(reverse_lazy('core:index'))
        self.assertEqual(200, response.status_code)
        self.assertContains(response, "Python Academy Forum")

    def test_index_view_after_login(self):
        user = self.get_or_create_user()
        self.client.force_login(user)

        response = self.client.get(reverse_lazy('core:index'))
        self.assertEqual(200, response.status_code)
        self.assertContains(response, f"Profile {user.username}")
        self.assertContains(response, "Logout")
        self.assertContains(response, "Change password")
        self.assertNotContains(response, "Login")

    def test_index_without_categories(self):
        response = self.client.get(reverse_lazy('core:index'))
        self.assertEqual(200, response.status_code)
        self.assertContains(response, "No categories")
