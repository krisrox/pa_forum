from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.db import transaction
from django.shortcuts import render, redirect
from django.views.generic import CreateView, DeleteView, FormView, ListView, UpdateView
from django.urls import reverse_lazy, reverse
from django.utils.translation import gettext_lazy as _
from .forms import CreateCategoryForm, RegisterForm, TopicForm, PostForm
from .mixins import SuperUserMixin
from .models import Category, Topic, Post


class IndexView(ListView):
    """ Index view """
    model = Category
    template_name = "index.html"


index_view = IndexView.as_view()


class CreateCategoryView(LoginRequiredMixin, SuperUserMixin, SuccessMessageMixin, CreateView):
    """ Create Category view """
    model = Category
    form_class = CreateCategoryForm
    template_name = "create_category.html"
    success_url = reverse_lazy('core:index')
    success_message = _("Category successfully created")


create_category_view = CreateCategoryView.as_view()


class DeleteCategoryView(LoginRequiredMixin, SuperUserMixin, SuccessMessageMixin, DeleteView):
    """  Delete category view """
    model = Category
    template_name = "delete_category.html"
    success_url = reverse_lazy('core:index')
    success_message = _("Category successfully deleted")


delete_category_view = DeleteCategoryView.as_view()


class UpdateCategoryView(LoginRequiredMixin, SuperUserMixin, SuccessMessageMixin, UpdateView):
    """ Update category view """
    model = Category
    form_class = CreateCategoryForm
    template_name = "create_category.html"
    success_url = reverse_lazy('core:index')
    success_message = _("Category successfully updated")


update_category_view = UpdateCategoryView.as_view()


class TopicListView(ListView):
    """ List of topics from specified categories """
    model = Topic
    template_name = "topic_list.html"
    paginate_by = 20

    def get_queryset(self):
        """ Override queryset """
        category_pk = self.kwargs['pk']
        return Topic.objects.filter(category_id=category_pk)

    def get_context_data(self, *, object_list=None, **kwargs):
        """ Extend get context data """
        context = super(TopicListView, self).get_context_data(object_list=object_list, **kwargs)
        context['category'] = Category.objects.get(id=self.kwargs['pk'])
        return context


topic_list_view = TopicListView.as_view()


class TopicCreateView(LoginRequiredMixin, SuccessMessageMixin, FormView):
    """ Create new topic """
    model = Topic
    form_class = TopicForm
    model2 = Post
    form_class2 = PostForm
    template_name = "create_topic.html"
    success_message = _("Topic created with success")
    success_url = reverse_lazy("core:index")

    def get_context_data(self, **kwargs):
        """ Extend default context """
        context = super(TopicCreateView, self).get_context_data(**kwargs)
        context['form2'] = self.form_class2()
        return context

    def post(self, request, *args, **kwargs):
        """ Extend post method """
        response = super(TopicCreateView, self).post(request, *args, **kwargs)
        form = self.form_class({'name': request.POST['name']})
        form2 = self.form_class2({'text': request.POST['text']})

        if form.is_valid() and form2.is_valid():
            with transaction.atomic():
                new_topic = self.model(name=request.POST['name'], category_id=self.kwargs['pk'])
                new_topic.save()
                first_post = self.model2(text=request.POST['text'], topic=new_topic, author=request.user)
                first_post.save()
            return redirect(self.success_url)

        return render(request, self.template_name, {'form': form, 'form2': form2})


topic_create_view = TopicCreateView.as_view()


class TopicPostsView(ListView):
    """ List of posts from topic """
    model = Post
    template_name = "topic_posts.html"
    form_class = PostForm
    paginate_by = 20

    def get_queryset(self):
        """ Override queryset """
        topic_pk = self.kwargs['pk']
        return Post.objects.filter(topic_id=topic_pk)

    def get_context_data(self, *, object_list=None, **kwargs):
        """ Extend get context data """
        context = super(TopicPostsView, self).get_context_data(object_list=object_list, **kwargs)
        context['topic'] = Topic.objects.get(id=self.kwargs['pk'])
        context['form'] = self.form_class()
        return context


topic_posts_view = TopicPostsView.as_view()


class CreatePostView(LoginRequiredMixin, CreateView):
    """ Create new post by authenticated user """
    model = Post
    form_class = PostForm
    success_message = _("Post created with success")

    def get(self, *args, **kwargs):
        """ Override method """
        return redirect(
            reverse('core:topic_posts', kwargs=kwargs)
        )

    def post(self, request, *args, **kwargs):
        """ Override method """
        form = self.form_class({'text': request.POST['text']})
        if not form.is_valid():
            messages.error(request, _("Incorrect data in post form!"))
            return redirect(
                reverse('core:topic_posts', kwargs=kwargs)
            )
        post = self.model(text=request.POST['text'], topic_id=kwargs['pk'], author=request.user)
        post.save()
        messages.success(request, self.success_message)
        return redirect(
            reverse('core:topic_posts', kwargs=kwargs)
        )


create_post_view = CreatePostView.as_view()


class RegisterView(SuccessMessageMixin, CreateView):
    """ Create new user """
    form_class = RegisterForm
    success_url = reverse_lazy('core:index')
    template_name = "registration/register.html"
    success_message = _("User successfully created")


register_view = RegisterView.as_view()